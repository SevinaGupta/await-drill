const test = require("../problem1.js")
let path =require('path')
const dir = "../dir"
createPath = path.join(__dirname,dir)

async function createAndDeleteFiles(dir,createPath){
	await test.createRandomDirctory(createPath)
	await test.createJsonFile("../dir/jsonFile.json","welcome to await")
	await test.deleteFiles("../dir/jsonFile.json")
}

createAndDeleteFiles(dir,createPath)