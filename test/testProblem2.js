const test = require('../problem2.js')
const path = require('path')


async function fileRead (file){

	await test.writeFileInUpperCase("../lipsum.txt","../fileOfUpperCase.txt")
	await test.writeSplitDataInLowerCase("../fileOfUpperCase.txt","../splitDataInLowerCase.txt")
	await test.writeSortDataInFile("../splitDataInLowerCase.txt","../sortData.txt")
	await test.deleteAllFiles("filenames.txt")
}

fileRead("../lipsum.txt")