const fs = require('fs')
const path = require('path')

async function fileRead(file) {
    return data = fs.promises.readFile(file, 'utf-8')
}


async function writeFileInUpperCase(fileRead, writeFile) {
        return data = await fs.promises.readFile(fileRead, 'utf-8')
            .then((data) => {
				let dataInUpperCase = data.toString().toUpperCase()
                fs.promises.writeFile(writeFile, dataInUpperCase)
				.then(() => {
                        console.log("file convert into upperCase!")
                        fs.promises.appendFile('../test/filenames.txt', writeFile.toString() + '\n')
                            .then(() => console.log("uppercase file name append."))
                            .catch((err) => console.log(err))
                    })
                    .catch((err) => console.log(err))
            })
}


async function writeSplitDataInLowerCase(readfile, writefile) {
        return data = await fs.promises.readFile(readfile)
            .then((data) => {
                let dataInLower = data.toString().toLowerCase();
                splitData = dataInLower.split(".").join("\n");
                fs.promises.writeFile(writefile, splitData, 'utf-8')
                    .then(() => {
                        console.log("Data split into sentences in lowerCase!")
                        fs.promises.appendFile('../test/filenames.txt', writefile.toString() + '\n')
                            .then(() => console.log("split file name append."))
                            .catch((err) => console.log(err))
                    })
                    .catch((err) => console.log(err))
            })
}


async function writeSortDataInFile(fileread, filewrite) {
        return data = await fs.promises.readFile(fileread)
            .then((data) => {
                data = data.toString();
                let sortdata = data.split('\n').sort().join('\n');
                fs.promises.writeFile(filewrite, sortdata, 'utf-8')
                    .then(() => {
                        console.log("Data is Sorted!")
                        fs.promises.appendFile('../test/filenames.txt', filewrite.toString() + '\n')
                            .then(() => console.log("sort file name append."))
                            .catch((err) => console.log(err))
                    })
                    .catch((err) => console.log(err))
            })
}


async function deleteAllFiles(readfile) {
         data = await fs.promises.readFile(readfile)
            .then((data) => {
                fileNameArray = data.toString().trim().split("\n")
                fileNameArray.forEach(data => {
                    fs.promises.unlink(data)
                        .then(() => console.log("All file deleted!"))
                        .catch((err) => console.log(err))
                })
            })
}

module.exports = {fileRead, writeFileInUpperCase,writeSplitDataInLowerCase,writeSortDataInFile,deleteAllFiles}